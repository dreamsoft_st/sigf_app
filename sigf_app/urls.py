# coding=utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

	url(r'^$', 'proyecto.views.proyecto_table', name='home'),
	url(r'^proyecto/agregar/$', 'proyecto.views.agregarproyecto_view', name='agregar_proyecto'),
	url(r'^proyecto/ver/([-\w]+)/$', 'proyecto.views.proyecto_view', name='ver_proyecto'),
	url(r'^proyecto/comentarios/([-\w]+)/$', 'proyecto.views.comentarios_view', name='comentarios_proyecto'),

	url(r'^activar/([-\w]+)/(.*)\/$', 'usuario.views.valida_view', name='activar'),

	url(r'^login/$', 'usuario.views.login_view', name='login'),
	url(r'^logout/$', 'usuario.views.logout_view', name='logout'),
	url(r'^registro/$', 'usuario.views.registro_view', name='registro'),
	url(r'^editar/$', 'usuario.views.cambiar_contrasena', name='editar'),

    url(r'^admin/', include(admin.site.urls)),
] #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))