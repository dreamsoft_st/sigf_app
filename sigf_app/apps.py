from suit.apps import DjangoSuitConfig

class MyDjangoSuitConfig(DjangoSuitConfig):
	admin_name = 'SuKarne ~ SIGF'
	menu_position = 'horizontal'
	form_size = 'col-xs-12 col-sm-2:col-xs-12 col-sm-10'