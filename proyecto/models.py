# coding=utf-8
from django.contrib.auth.models import User
from django.db import models

from django.template.defaultfilters import slugify
from usuario.models import Area

class Fondo(models.Model):
	nombre = models.CharField(max_length=100)
	letra = models.CharField(max_length=2)

	def __unicode__(self):
		return self.nombre

class RazonSocial(models.Model):
	nombre = models.CharField(max_length=100)

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = 'Razon Social'

class Rubro(models.Model):
	nombre = models.CharField(max_length=100)

	def __unicode__(self):
		return self.nombre

class Proyecto(models.Model):
	usuario = models.ForeignKey(User, related_name='usuario')
	analista = models.ForeignKey(User, related_name='analista', null=True)
	folio_interno = models.CharField(max_length=50)
	nombre = models.CharField(max_length=100, unique=True)
	descripcion = models.TextField()
	#estatus = models.CharField(max_length=100)
	inversion = models.DecimalField(max_digits=11, decimal_places=2)
	tipo_moneda = models.CharField(max_length=100)
	fecha = models.DateTimeField(auto_now_add=True)
	area_responsable = models.ForeignKey(Area)

	duracion = models.IntegerField()
	fecha_inicio = models.DateField()

	fondo = models.ForeignKey(Fondo, null=True, blank=True)
	razon_social = models.ForeignKey(RazonSocial)
	folio_externo = models.CharField(max_length=50, blank=True)

	rubros_apoyo = models.CharField(max_length=200)

	slug = models.SlugField(max_length=130, blank=True, editable=False)

	def anexos(self):
		return Anexo.objects.filter(proyecto=self.pk)

	def status(self):
		return Status_Proyecto.objects.filter(proyecto = self.pk).order_by('-status__fase').first()

	def all_status(self):
		return Status_Proyecto.objects.filter(proyecto = self.pk).order_by('status__fase').all()

	def __unicode__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		super(Proyecto, self).save(*args, **kwargs)
		if not self.slug:
			self.slug = slugify(self.nombre[:90]+str(self.id))
			self.save()


class Mensaje(models.Model):
	proyecto = models.ForeignKey(Proyecto)
	usuario = models.ForeignKey(User, related_name='usuario_m')
	comentario = models.TextField()
	fecha = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.proyecto.nombre

class Anexo(models.Model):
	proyecto = models.ForeignKey(Proyecto)
	analista = models.ForeignKey(User)
	nombre = models.CharField(max_length=200)
	url_archivo = models.URLField()
	fecha = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "%s - %s" % (self.proyecto.nombre, self.nombre)

class Status(models.Model):
	fase = models.IntegerField(unique=True)
	nombre = models.CharField(max_length=150, unique=True)

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = 'Status'

class Status_Proyecto(models.Model):
	proyecto = models.ForeignKey(Proyecto)
	status = models.ForeignKey(Status)
	fecha = models.DateTimeField(auto_now_add=True)
	fecha_entrega = models.DateField(null=True, blank=True)
	comentario = models.CharField(max_length=200,null=True, blank=True)
	usuario = models.ForeignKey(User, null=True, blank=True, editable=False)


	def __unicode__(self):
		return "%s - %s" % (self.proyecto.nombre, self.status.nombre)

	class Meta:
		verbose_name_plural = 'Status Proyecto'