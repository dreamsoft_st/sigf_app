# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('usuario', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Anexo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200)),
                ('url_archivo', models.URLField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('analista', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Fondo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('letra', models.CharField(max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='Mensaje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comentario', models.TextField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Proyecto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('folio_interno', models.CharField(max_length=50)),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.TextField()),
                ('estatus', models.CharField(max_length=100)),
                ('inversion', models.DecimalField(max_digits=11, decimal_places=2)),
                ('tipo_moneda', models.CharField(max_length=100)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('duracion', models.IntegerField()),
                ('fecha_inicio', models.DateField()),
                ('folio_externo', models.CharField(max_length=50, blank=True)),
                ('autorizado_cisk', models.CharField(max_length=20, blank=True)),
                ('autorizado_fiscal', models.CharField(max_length=20, blank=True)),
                ('cancelado', models.BooleanField(default=False)),
                ('aceptado', models.BooleanField(default=False)),
                ('rubros_apoyo', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=130, editable=False, blank=True)),
                ('analista', models.ForeignKey(related_name='analista', to=settings.AUTH_USER_MODEL, null=True)),
                ('area_responsable', models.ForeignKey(to='usuario.Area')),
                ('fondo', models.ForeignKey(blank=True, to='proyecto.Fondo', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RazonSocial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Razon Social',
            },
        ),
        migrations.CreateModel(
            name='Rubro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='proyecto',
            name='razon_social',
            field=models.ForeignKey(to='proyecto.RazonSocial'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='usuario',
            field=models.ForeignKey(related_name='usuario', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mensaje',
            name='proyecto',
            field=models.ForeignKey(to='proyecto.Proyecto'),
        ),
        migrations.AddField(
            model_name='mensaje',
            name='usuario',
            field=models.ForeignKey(related_name='usuario_m', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='anexo',
            name='proyecto',
            field=models.ForeignKey(to='proyecto.Proyecto'),
        ),
    ]
