# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='proyecto',
            name='aceptado',
        ),
        migrations.RemoveField(
            model_name='proyecto',
            name='autorizado_cisk',
        ),
        migrations.RemoveField(
            model_name='proyecto',
            name='autorizado_fiscal',
        ),
        migrations.RemoveField(
            model_name='proyecto',
            name='cancelado',
        ),
    ]
