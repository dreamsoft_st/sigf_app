# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0006_auto_20150716_1643'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='status',
            options={'verbose_name_plural': 'Status'},
        ),
        migrations.AlterModelOptions(
            name='status_proyecto',
            options={'verbose_name_plural': 'Status Proyecto'},
        ),
        migrations.RemoveField(
            model_name='proyecto',
            name='estatus',
        ),
    ]
