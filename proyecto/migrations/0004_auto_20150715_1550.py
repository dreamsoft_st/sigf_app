# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0003_status_status_proyecto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='fase',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='status',
            name='nombre',
            field=models.CharField(unique=True, max_length=150),
        ),
    ]
