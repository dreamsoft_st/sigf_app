# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0004_auto_20150715_1550'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='status_proyecto',
            name='dias',
        ),
        migrations.AddField(
            model_name='status_proyecto',
            name='comentario',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='status_proyecto',
            name='fecha_entrega',
            field=models.DateField(null=True, blank=True),
        ),
    ]
