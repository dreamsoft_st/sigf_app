# coding=utf-8
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from .models import Proyecto, Rubro, RazonSocial, Mensaje, Anexo, Fondo, Status, Status_Proyecto
from usuario.models import Area

from django.forms import ModelForm
from django.contrib import messages
from django.contrib.messages import get_messages

from datetime import date
import re, locale

from django.core.mail import send_mail
from django.conf import settings

from django.contrib.auth.models import User
from django.utils.encoding import smart_unicode

@login_required()
def proyecto_view(request, slug):
	fondo = Fondo.objects.order_by('nombre')

	if request.method == 'POST':
		proyecto_id = request.POST['proyecto_id']
		accion = request.POST['accion']
		if accion == 'cancelar':
			ult_status = Status_Proyecto.objects.filter(proyecto__id=proyecto_id).order_by('-status__fase').first()
			if ult_status.status == get_object_or_404(Status, fase = 1):
				sp = Status_Proyecto()
				sp.proyecto = get_object_or_404(Proyecto, pk = proyecto_id)
				sp.status = get_object_or_404(Status,fase = 99)
				sp.usuario = request.user
				sp.save()
			elif request.user.informacion.rol.nombre == 'Gerente' or Proyecto.objects.filter(analista = request.user, pk = proyecto_id).exists():
				sp = Status_Proyecto()
				sp.proyecto = get_object_or_404(Proyecto, pk = proyecto_id)
				sp.status = get_object_or_404(Status,fase = 99)
				sp.usuario = request.user
				sp.save()

		elif accion == 'agregar_anexo':
			
			if Proyecto.objects.filter(analista = request.user, pk = proyecto_id).exists() or request.user.informacion.rol.nombre == 'Gerente':
				a = Anexo()
				a.analista = request.user
				a.nombre = request.POST['nombre_archivo']
				a.url_archivo = request.POST['url_archivo']
				a.proyecto = get_object_or_404(Proyecto, pk=proyecto_id)
				a.save()
		elif accion == 'eliminar_anexo':

			if Proyecto.objects.filter(analista = request.user, pk = proyecto_id).exists() or request.user.informacion.rol.nombre == 'Gerente':
				Anexo.objects.filter(pk=request.POST['anexo_id']).delete()

		elif accion == 'agregar_datos_ad':

			folio_externo = request.POST['folio_externo']
			organismo_fondo = None
			if len(request.POST['organismo_fondo']) > 0:
				organismo_fondo = get_object_or_404(Fondo, pk = request.POST['organismo_fondo'])
				pro = get_object_or_404(Proyecto, pk=proyecto_id)
				folio_interno = pro.folio_interno
				folio_interno = re.sub('\((.*?)\)', '(' + str(organismo_fondo.letra) + ')',folio_interno)

			status = request.POST['status']
			status_fecha = request.POST['status_fecha']
			status_comen = request.POST['status_comen']
			

			if Proyecto.objects.filter(analista = request.user, pk = proyecto_id).exists() or request.user.informacion.rol.nombre == 'Gerente':
				
				proyecto = get_object_or_404(Proyecto, pk = proyecto_id)
				stat_nuevo = Status.objects.filter(fase=status).first()
				ult_status = proyecto.status()
				if ult_status is None:
					ult_status = 0
				
				
				if stat_nuevo is not None:
					if (stat_nuevo.fase < ult_status.status.fase):
						Status_Proyecto.objects.filter(proyecto__id=proyecto_id, status__fase__gte=stat_nuevo.fase).delete()

					if (proyecto.status() is not None and ult_status.status.fase != stat_nuevo.fase):
						send_mail('Cambio Estatus - ' + smart_unicode(proyecto.nombre), 'Cambio de estatus en el Proyecto.', settings.DEFAULT_FROM_EMAIL, [proyecto.usuario.email], fail_silently=False, html_message='Cambio de estatus en el Proyecto.<p>' + stat_nuevo.nombre + '</p>')

					if not (Status_Proyecto.objects.filter(proyecto=proyecto, status=stat_nuevo).exists()):
						sp = Status_Proyecto()
						sp.proyecto = proyecto
						sp.status = stat_nuevo
						sp.usuario = request.user
						if (status_fecha != ''):
							sp.fecha_entrega = status_fecha
						sp.comentario = status_comen
						sp.save()

				if (organismo_fondo is not None and folio_externo != '') :
					Proyecto.objects.filter(pk = proyecto_id).update(fondo = organismo_fondo, folio_interno = folio_interno, folio_externo = folio_externo)
					
				elif (organismo_fondo is not None):
					Proyecto.objects.filter(pk = proyecto_id).update(fondo = organismo_fondo, folio_interno = folio_interno)
					
				elif (folio_externo != ''):
					Proyecto.objects.filter(pk = proyecto_id).update(folio_externo = folio_externo)

		elif accion == 'estatus':
			proyecto = get_object_or_404(Proyecto, pk = proyecto_id)
			ult_status = proyecto.status()
			if not (ult_status.status.fase == 4):
				sp = Status_Proyecto()
				sp.proyecto = proyecto
				sp.status = get_object_or_404(Status, fase = 4)
				sp.usuario = request.user
				sp.save()
				send_mail('Cambio Estatus - ' + smart_unicode(proyecto.nombre), 'Cambio de estatus en el Proyecto.', settings.DEFAULT_FROM_EMAIL, [proyecto.analista.email], fail_silently=False, html_message='Cambio de estatus en el Proyecto.<p>Aceptado por CISK</p>')

	proyecto = get_object_or_404(Proyecto, slug = slug)

	proyecto.folio_interno = proyecto.folio_interno.replace('(', '')
	proyecto.folio_interno = proyecto.folio_interno.replace(')', '')
	locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
	proyecto.inversion = locale.currency(proyecto.inversion, grouping=True)

	status = Status.objects.order_by('fase')
	return render(request, 'proyecto.html', { 'proyecto': proyecto, 'fondo': fondo, 'status' : status})

@login_required()
def proyecto_table(request):
	if request.user.informacion.rol.nombre == 'Gerente':
		if request.user.informacion.area == None: 
			proyectos = Proyecto.objects.order_by('fecha')
		else:
			proyectos = Proyecto.objects.filter(area_responsable=request.user.informacion.area).order_by('fecha')
	else:
		proyectos = Proyecto.objects.filter(Q(usuario=request.user) | Q(analista=request.user))

	if request.method == 'POST':
		proyecto_id = request.POST['proyecto_id']
		accion = request.POST['accion']
		ult_status = Status_Proyecto.objects.filter(proyecto__id=proyecto_id).order_by('-status__fase').first()
		if accion == 'cancelar':
			if (ult_status.status == get_object_or_404(Status, fase = 1)):
				sp = Status_Proyecto()
				sp.proyecto = get_object_or_404(Proyecto, pk = proyecto_id)
				sp.status = get_object_or_404(Status, fase = 99)
				sp.usuario = request.user
				sp.save()
			elif request.user.informacion.rol.nombre == 'Gerente' or Proyecto.objects.filter(analista = request.user, pk = proyecto_id).exists():
				sp = Status_Proyecto()
				sp.proyecto = get_object_or_404(Proyecto, pk = proyecto_id)
				sp.status = get_object_or_404(Status, fase = 99)
				sp.usuario = request.user
				sp.save()

	proyectoss = []
	for p in proyectos:
		p.folio_interno = p.folio_interno.replace('(', '')
		p.folio_interno = p.folio_interno.replace(')', '')
		proyectoss.append( p )

	return render(request, 'tabla_proyectos.html', { 'proyectos': proyectoss })

@login_required()
def agregarproyecto_view(request):
	if request.user.informacion.rol.nombre != 'Cliente Interno':
		return redirect('/')

	if request.method == 'POST':
		nombre = request.POST['nombre']
		descripcion = request.POST['descripcion']
		rubro = request.POST['rubro_otro']
		area = request.POST['area']
		razon = request.POST['razon']
		inversion = request.POST['inversion']
		moneda = request.POST['moneda']
		fecha = request.POST['fecha']
		duracion = request.POST['duracion']

		area = get_object_or_404(Area, pk=int(area))
		razon = get_object_or_404(RazonSocial, pk=int(razon))
		solicitud = Proyecto.objects.last()
		if solicitud is None:
			solicitud = '01'
		elif solicitud.id < 10:
			solicitud = '0' + str(solicitud.id)
		else:
			solicitud = str(solicitud.id)

		rubro_p = ''
		for rub in request.POST.getlist('rubros'):
			rubro_p = rub + ',' + rubro_p
		if len(rubro) > 0:
			rubro_p = rubro_p + rubro
		elif len(rubro_p) > 0:
			rubro_p = rubro_p[:-1]

		pro = Proyecto()
		pro.nombre = nombre
		pro.descripcion = descripcion
		pro.area_responsable = area
		pro.razon_social = razon
		pro.inversion = inversion
		pro.tipo_moneda = moneda
		pro.fecha_inicio = fecha
		pro.duracion = duracion
		pro.folio_interno = 'SGF-' + str(date.today().year) + '-(X)' + str(solicitud)
		pro.usuario = request.user
		pro.rubros_apoyo = rubro_p
		pro.save()

		status_sa = get_object_or_404(Status, fase = 1)
		sp = Status_Proyecto()
		sp.proyecto = pro
		sp.status = status_sa
		sp.usuario = request.user
		sp.save()

		emails = []
		usrs = User.objects.filter(informacion__rol__nombre = 'Gerente')
		for usr in usrs:
			emails.append( usr.email )

		if len(emails) > 0:
			send_mail('Nuevo Proyecto - ' + smart_unicode(pro.nombre), 'Nuevo Proyecto - ' + smart_unicode(pro.nombre) + '<p>' + str(pro.usuario.first_name) + ' ' + str(pro.usuario.last_name) + '</p>', settings.DEFAULT_FROM_EMAIL, emails, fail_silently=False, html_message='Nuevo Proyecto - ' + smart_unicode(pro.nombre) + '<p>' + str(pro.usuario.first_name) + ' ' + str(pro.usuario.last_name) + '</p>')
		
		messages.success(request, 'Su Proyecto se ha registrado de manera exitosa en la Base de Datos, en breve se le asignará un Analista de Fondos que lo atenderá en el proyecto')
		return redirect('/')

	rubros = Rubro.objects.order_by('nombre')
	areas = Area.objects.order_by('nombre')
	razones = RazonSocial.objects.order_by('nombre')
	return render(request, 'agregar_proyecto.html', {
		'rubros': rubros,
		'areas': areas,
		'razones': razones
	})

@login_required()
def comentarios_view(request, slug):
	if request.method == 'POST':
		proyecto_id = request.POST['proyecto_id']
		comentario = request.POST['comentario']

		m = Mensaje()
		m.proyecto = get_object_or_404(Proyecto, pk=proyecto_id)
		m.comentario = comentario
		m.usuario = request.user
		m.save()

		recibe = m.proyecto.analista.email
		if m.proyecto.usuario != request.user:
			recibe = m.proyecto.usuario.email

		send_mail('Nuevo Comentario - ' + smart_unicode(m.proyecto.nombre), 'Nuevo Mensaje en el Proyecto.', settings.DEFAULT_FROM_EMAIL, [recibe], fail_silently=False, html_message='Nuevo Mensaje en el Proyecto.<p>' + m.comentario + '</p>')

	proyecto = get_object_or_404(Proyecto, slug = slug)
	if proyecto.analista is None:
		messages.error(request, 'Aun no tienes un analista asignado')
		return redirect('/')

	mensajes = Mensaje.objects.filter(proyecto = proyecto )
	return render(request, 'comentarios.html', { 'mensajes': mensajes, 'proyecto': proyecto })
