from django.contrib import admin
from django.contrib.auth.models import User

from .models import Fondo, RazonSocial, Rubro, Mensaje, Anexo, Proyecto, Status, Status_Proyecto

@admin.register(Fondo)
class FondoAdmin(admin.ModelAdmin):
	list_display = ['nombre',]

@admin.register(RazonSocial)
class RazonSocialAdmin(admin.ModelAdmin):
	list_display = ['nombre',]

@admin.register(Rubro)
class RubroAdmin(admin.ModelAdmin):
	list_display = ['nombre',]

@admin.register(Mensaje)
class MensajeAdmin(admin.ModelAdmin):
	list_display = ['proyecto', 'usuario', 'fecha']

@admin.register(Anexo)
class AnexoAdmin(admin.ModelAdmin):
	list_display = ['proyecto', 'analista', 'nombre', 'fecha']

@admin.register(Proyecto)
class ProyectoAdmin(admin.ModelAdmin):
	list_display = ['folio_interno', 'nombre', 'usuario', 'analista', 'Status', 'fecha']

	def Status(self, obj):
		stat = ''
		ult_stat = Status_Proyecto.objects.filter(proyecto = obj.pk).order_by('-status__fase').first()
		if ult_stat is not None:
			stat = ult_stat.status.nombre
		return stat

	def get_form(self, request, obj=None, **kwargs):
		form = super(ProyectoAdmin,self).get_form(request, obj,**kwargs)
		form.base_fields['analista'].queryset = form.base_fields['analista'].queryset.filter(informacion__rol__nombre='Analista')
		form.base_fields['usuario'].queryset = form.base_fields['usuario'].queryset.filter(informacion__rol__nombre='Cliente Interno')
		return form

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
	list_display = ['fase', 'nombre']
	
@admin.register(Status_Proyecto)
class Status_ProyectoAdmin(admin.ModelAdmin):
	list_display = ['status', 'proyecto']