from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Informacion, Area, Rol

@admin.register(Rol)
class RolAdmin(admin.ModelAdmin):
	list_display = ['nombre',]
	verbose_name_plural = 'Roles'

@admin.register(Area)
class AreaAdmin(admin.ModelAdmin):
	list_display = ['nombre',]

class InformacionInline(admin.StackedInline):
    model = Informacion
    can_delete = False
    verbose_name_plural = 'informacion'

class UserAdmin(UserAdmin):
    inlines = (InformacionInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)