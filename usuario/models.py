# coding=utf-8
from django.contrib.auth.models import User
from django.db import models

import uuid

from django.db.models.signals import post_save
from django.dispatch import receiver

# @receiver(post_save, sender=User)
# def create_favorites(sender, instance, created, **kwargs):
#     if created:
#         Informacion.objects.create(user=instance)

class Rol(models.Model):
	nombre = models.CharField(max_length=50)
	def __unicode__(self):
		return self.nombre

	def __unicode__(self):
		return "%s" % self.nombre

class Area(models.Model):
	nombre = models.CharField(max_length=50)
	def __unicode__(self):
		return self.nombre

	def __unicode__(self):
		return "%s" % self.nombre

class Informacion(models.Model):
	user = models.OneToOneField(User, null=True, blank=True)
	telefono = models.CharField(max_length=15, null=True, blank=True)
	no_empleado = models.IntegerField(null=True, blank=True)
	rol = models.ForeignKey(Rol, null=True, blank=True)
	area = models.ForeignKey(Area, null=True, blank=True)
	uuid = models.UUIDField(default=uuid.uuid4, editable=False)

	def __unicode__(self):
		if self.user is not None:
			return self.user.email
		return self.telefono
