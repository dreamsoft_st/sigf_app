# coding=utf-8
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.messages import get_messages

from django.contrib.auth.models import User
from .models import Informacion, Area, Rol

from django.core.mail import send_mail
from django.conf import settings

def cambiar_contrasena(request):
	if not request.user.is_authenticated():
		return redirect('/')
	if request.method == 'POST':
		pass_old = request.POST['pass_old']
		new_pass = request.POST['new_pass']
		new_pass2 = request.POST['new_pass2']
		if new_pass2 != new_pass:
			messages.error(request, 'Las contraseñas no coinciden')
		elif request.user.check_password(pass_old) == False:
			messages.error(request, 'La contraseña actual es incorrecta')
		else:
			request.user.set_password(new_pass)
			request.user.save()
			messages.success(request, 'Contraseña Cambiada')
			return redirect('/')
	return render(request, 'cambiar_pass.html')

def valida_view(request, uuid, email):
	if Informacion.objects.filter(uuid = uuid, user__email = email, user__is_active = False).exists():
		u = User.objects.get(email = email)
		u.is_active = True
		u.save()
		messages.success(request, 'Cuenta Activada')
	return redirect('/')

def logout_view(request):
	logout(request)
	return redirect('/')

def login_view(request):
	if request.user.is_authenticated():
		return redirect('/')

	if request.method == 'POST':
		email = request.POST['email']
		password = request.POST['password']
		
		user = authenticate( username=email, password=password )
		if user is not None:
			login(request, user)
			return redirect('/')
		else:			
			messages.error(request, 'Usuario/Contraseña incorrecta')
	areas = Area.objects.order_by('nombre')

	clase = 'success'
	storage = messages.get_messages(request)
	for message in storage:
		if message.tags == 'error' and clase == 'success':
			clase = 'danger'
	storage.used = False
	return render(request, 'login.html', {'areas': areas, 'clase': clase})

def registro_view(request):
	if request.user.is_authenticated():
		return redirect('/')

	if request.method == 'POST':
		nombre = request.POST['nombre']
		apellido = request.POST['apellido']
		email = request.POST['email']
		password = request.POST['password']
		telefono = request.POST['telefono']
		no_empleado = request.POST['no_empleado']
		area = request.POST['area']
		
		exito = True
		
		if len(password) < 5:
			messages.error(request, 'La contraseña no debe ser menor de 5 caracteres')
			exito = False
		if len(nombre) == 0:
			messages.error(request, 'El nombre no debe estar vacio')
			exito = False
		if len(area) == 0:
			messages.error(request, 'El area resposanble no debe estar vacio')
			exito = False
		if len(apellido) == 0:
			messages.error(request, 'El apellido no debe estar vacio')
			exito = False
		if len(telefono) != 0 and telefono.isdigit() == False:
			messages.error(request, 'El telefono debe ser solo numeros')
			exito = False
		if len(no_empleado) != 0 and no_empleado.isdigit() == False:
			messages.error(request, 'El No. de Empleado debe ser solo numeros')
			exito = False
		if len(email.split('@')) < 0 or len(email) == 0:
			messages.error(request, 'Ingrese un correo valido')
			exito = False
		elif User.objects.filter(email=email).exists():
			messages.error(request, 'Ya existe un usuario registrado con ese email')
			exito = False

		if exito:
			user = User()
			user.username = email
			user.email = email
			user.first_name = nombre
			user.last_name = apellido
			user.set_password(password)
			user.is_active = False
			user.save()

			rol = Rol.objects.last()
			area = Area.objects.get(pk = int(area))
			u = Informacion(user = user, telefono = telefono, no_empleado = no_empleado, rol = rol, area = area)
			u.save()

			messages.success(request, 'Registro Exitoso, porfavor valida tu correo')
			send_mail('Registro SIGF - SuKarne', 'Para Activar la cuenta debes entrar en el siguiente link http://' + request.get_host() + '/activar/' + str(u.uuid) + '/' + str(user.email) + '/' , settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message='Para Activar la cuenta debes entrar en el siguiente link <p><a href="http://' + request.get_host() + '/activar/' + str(u.uuid) + '/'  + str(user.email) + '/">http://' + request.get_host() + '/activar/' + str(u.uuid) + '/'  + str(user.email) + '/</a></p>')

			#user = authenticate( username=email, password=password )
			#login(request, user)
	
	return redirect('/')