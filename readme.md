# SIGF (Sistema Integral de Gestión de Fondos)

SIGF was developed for the control and automation of the registration process and monitoring of projects in federal funds for the company in the direction SuKarne fund management.

This project includes:
  - Managment of projects
  - Managment users
  - Comunication with the project analyst (Internal Customer - Analyst)

Benefices:
 - Internal Customer.- Can see the status and progress of their projects, obtaining information from each of them and see the accompanying documentation or charged by their analyst, and view the log history of each project.
  
 - Analyst.- This person is responsible for keeping track and progress of each project that he was assigned, also as having communication with your internal customer and his area manager, informing to them the advances that are taking place in every project, this automatically by mail.
  
 - Manager.- This person is responsible for overseeing all projects in your area, in addition to assigning one analyst to each project, who can take control and progress of the project if required if not the manager can do this.

### Tech

SIGF uses a number of open source projects to work properly:

* [Python] - Backend
* [Django] - Framework for web develop!
* [Django Suit] - Beautiful Panel Admin
* [FontAwesome] - Awesomes Icons.
* [Bootstrap] - great UI boilerplate for modern web apps
* [Chosen.js] - Is a jQuery plugin that makes long, unwieldy select boxes much more user-friendly.
* [jQuery] 

### Installation

SIGF requires [Python 2.7][Django 1.8].

Install Django and Django-Suit:
```
$ pip install Django==1.8
$ pip install git+https://github.com/darklow/django-suit.git@bs3
```

Now you should configure the settings.py with you information.
 - DATABASES CONNECTIONS
 - EMAIL

Migration and dump data:
```
$ python manage.py migrate
$ python manage.py loaddata dump.json
```

Start the server.

```sh
$ python manage.py runserver
```

Login:
[Super Administrator]

    user: victor
    password: V123123123
    Admin Panel -> http://localhost:8000/admin
    
On the web for normal users they access by their email.

![1.png](https://bitbucket.org/repo/xxg7B5/images/1316523734-1.png)

![2.png](https://bitbucket.org/repo/xxg7B5/images/2007584890-2.png)
    
![3.png](https://bitbucket.org/repo/xxg7B5/images/3283672342-3.png)

Panel

![4.png](https://bitbucket.org/repo/xxg7B5/images/2024258378-4.png)

![5.png](https://bitbucket.org/repo/xxg7B5/images/1714044314-5.png)

   [Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [Python]: <https://www.python.org/>
   [Django]: <https://www.djangoproject.com/>
   [Chosen.js]: <https://harvesthq.github.io/chosen/>
   [FontAwesome]: <http://fontawesome.io/>